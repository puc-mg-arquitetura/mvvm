import 'package:counter_mvp/model/todo.dart';
import 'package:counter_mvp/repository/todo_repository.dart';
import 'package:flutter/cupertino.dart';

class TodoController with ChangeNotifier {
  final TodoRepository _repository;

  List<Todo> _todos = [];

  TodoController({required TodoRepository repository}) : _repository = repository;

  // Fetch todos from the repository
  Future<void> fetchTodos() async {
    _todos = await _repository.fetchTodos();
    notifyListeners();
  }

  // Accessor to get the current list of todos
  List<Todo> get todos => _todos;

  // Add a todo
  Future<void> addTodo(String title) async {
    final todo = Todo(id: _todos.length + 1, title: title);
    await _repository.addTodo(todo);
    _todos.add(todo);
    notifyListeners(); // Notify the UI
  }

  // Toggle the completion status of a todo
  Future<void> toggleTodo(Todo todo) async {
    todo.toggleDone();
    await _repository.updateTodo(todo);
    notifyListeners(); // Notify the UI
  }

  // Delete a todo
  Future<void> deleteTodo(Todo todo) async {
    await _repository.deleteTodo(todo);
    _todos.remove(todo);
    notifyListeners(); // Notify the UI
  }
}