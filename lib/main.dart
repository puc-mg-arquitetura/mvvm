import 'package:counter_mvp/controller/todo_controller.dart';
import 'package:counter_mvp/repository/in_memory_todo_repository.dart';
import 'package:counter_mvp/view/todo_list_view.dart';
import 'package:counter_mvp/view/todo_view.dart';
import 'package:counter_mvp/view_model/view_model.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  final todoRepository = InMemoryTodoRepository();
  MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        ChangeNotifierProvider(create: (_) => TodoViewModel(repository: todoRepository))
      ],
      child: MaterialApp(
        title: 'Flutter Demo',
        theme: ThemeData(
          colorScheme: ColorScheme.fromSeed(seedColor: Colors.deepPurple),
          useMaterial3: true,
        ),
        home: TodoListView(),
      ),
    );
  }
}
