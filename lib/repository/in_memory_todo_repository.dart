import 'package:counter_mvp/model/todo.dart';
import 'package:counter_mvp/repository/todo_repository.dart';

class InMemoryTodoRepository implements TodoRepository {
  List<Todo> _todos = [];
  int _counter = 1;

  @override
  Future<List<Todo>> fetchTodos() async => _todos;

  @override
  Future<void> addTodo(Todo todo) async {
    _todos.add(Todo(id: _counter++, title: todo.title, isDone: todo.isDone));
  }

  @override
  Future<void> toggleTodoStatus(int id) async {
    int? index = _todos.indexWhere((t) => t.id == id);
    if (index != null && index != -1) {
      _todos[index].isDone = !_todos[index].isDone;
    }
  }

  @override
  Future<void> deleteTodo(Todo todo) {
    // TODO: implement deleteTodo
    throw UnimplementedError();
  }

  @override
  Future<void> updateTodo(Todo todo) {
    // TODO: implement updateTodo
    throw UnimplementedError();
  }
}