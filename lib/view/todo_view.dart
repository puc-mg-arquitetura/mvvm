import 'package:counter_mvp/controller/todo_controller.dart';
import 'package:counter_mvp/model/todo.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class TodoList extends StatefulWidget {
  const TodoList({super.key});

  @override
  State<TodoList> createState() => _TodoListState();
}

class _TodoListState extends State<TodoList> {

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: const Text('Todo List')),
      body: Consumer<TodoController>(
        builder: (BuildContext context, TodoController _controller, Widget? child) => ListView.builder(
          itemCount: _controller.todos.length,
          itemBuilder: (context, index) {
            final todo = _controller.todos[index];
            return ListTile(
              title: Text(todo.title),
              trailing: Checkbox(
                value: todo.isDone,
                onChanged: (value) {
                  setState(() {
                    _controller.toggleTodo(todo);
                  });
                },
              ),
            );
          },
        ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: _showAddTodoDialog,
        child: const Icon(Icons.add),
      ),
    );
  }

  void _showAddTodoDialog() {
    TextEditingController _todoController = TextEditingController();

    showDialog(
      context: context,
      builder: (context) {
        return AlertDialog(
          title: Text('Add Todo'),
          content: TextField(
            controller: _todoController,
            decoration: InputDecoration(hintText: 'Enter todo title'),
          ),
          actions: <Widget>[
            TextButton(
              child: Text('Cancel'),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
            Consumer<TodoController>(
              builder: (BuildContext context, TodoController _controller, Widget? child) => TextButton(
                child: Text('Add'),
                onPressed: () {
                  final title = _todoController.text;
                  if (title.isNotEmpty) {
                    _controller.addTodo(title);
                    setState(() {});  // Rebuild the widget to reflect changes
                  }
                  Navigator.of(context).pop();
                },
              ),
            ),
          ],
        );
      },
    );
  }
}
