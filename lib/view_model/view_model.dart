import 'package:counter_mvp/model/todo.dart';
import 'package:counter_mvp/repository/todo_repository.dart';
import 'package:flutter/material.dart';

class TodoViewModel with ChangeNotifier {
  final TodoRepository repository;
  List<Todo> _todos = [];

  TodoViewModel({required this.repository});

  List<Todo> get todos => _todos;

  void loadTodos() async {
    _todos = await repository.fetchTodos();
    notifyListeners();
  }

  void addTodo(Todo todo) async {
    await repository.addTodo(todo);
    loadTodos();
  }

  void toggleTodoStatus(int id) async {
    await repository.toggleTodoStatus(id);
    loadTodos();
  }
}